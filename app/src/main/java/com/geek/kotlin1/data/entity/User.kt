package com.geek.kotlin1.data.entity

data class User(val name: String, val email: String)