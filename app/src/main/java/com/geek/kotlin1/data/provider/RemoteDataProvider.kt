package com.geek.kotlin1.data.provider

import com.geek.kotlin1.data.entity.Note
import com.geek.kotlin1.data.entity.User
import com.geek.kotlin1.data.model.NoteResult
import kotlinx.coroutines.channels.ReceiveChannel

interface RemoteDataProvider {
    fun subscribeToAllNotes(): ReceiveChannel<NoteResult>
    suspend fun getNoteById(id: String): Note
    suspend fun saveNote(note: Note): Note
    suspend fun getCurrentUser(): User?
    suspend fun deleteNote(noteId: String)
}