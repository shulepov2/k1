package com.geek.kotlin1.ui.splash

import com.geek.kotlin1.ui.base.BaseViewState

class SplashViewState(authenticated: Boolean? = null, error: Throwable? = null) :
    BaseViewState<Boolean?>(authenticated, error)