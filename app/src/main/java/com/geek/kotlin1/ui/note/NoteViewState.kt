package com.geek.kotlin1.ui.note

import com.geek.kotlin1.data.entity.Note
import com.geek.kotlin1.ui.base.BaseViewState

class NoteViewState(data: Data = Data(), error: Throwable? = null) :
    BaseViewState<NoteViewState.Data>(data, error) {
    data class Data(val isDeleted: Boolean = false, val note: Note? = null)
}