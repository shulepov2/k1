package com.geek.kotlin1.ui.note

import com.geek.kotlin1.data.entity.Note

data class NoteData(val isDeleted: Boolean = false, val note: Note? = null)