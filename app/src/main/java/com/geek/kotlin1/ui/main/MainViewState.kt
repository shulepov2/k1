package com.geek.kotlin1.ui.main

import com.geek.kotlin1.data.entity.Note
import com.geek.kotlin1.ui.base.BaseViewState

class MainViewState(val notes: List<Note>? = null, error: Throwable? = null) :
    BaseViewState<List<Note>?>(notes, error)