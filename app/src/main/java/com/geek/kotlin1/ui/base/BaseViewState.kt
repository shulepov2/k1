package com.geek.kotlin1.ui.base

open class BaseViewState<T>(val data: T, val error: Throwable?)