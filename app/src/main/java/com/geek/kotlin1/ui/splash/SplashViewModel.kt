package com.geek.kotlin1.ui.splash

import com.geek.kotlin1.data.NotesRepository
import com.geek.kotlin1.data.errors.NoAuthException
import com.geek.kotlin1.ui.base.BaseViewModel
import kotlinx.coroutines.launch

class SplashViewModel(private val notesRepository: NotesRepository) :
    BaseViewModel<Boolean?>() {

    fun requestUser() {
        launch {
            notesRepository.getCurrentUser()?.let {
                setData(true)
            } ?: setError(NoAuthException())
        }
    }
}