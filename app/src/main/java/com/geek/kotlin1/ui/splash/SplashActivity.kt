package com.geek.kotlin1.ui.splash

import android.os.Handler
import androidx.lifecycle.ViewModelProvider
import com.geek.kotlin1.ui.base.BaseActivity
import com.geek.kotlin1.ui.main.MainActivity
import org.koin.android.viewmodel.ext.android.viewModel

class SplashActivity :
    BaseActivity<Boolean?>() {                                                                                                                                                                                                                                                                                                            //Я копипастил код с урока и не заметил эту надпись

    override val model: SplashViewModel by viewModel()

    override val layoutRes: Int? = null

    override fun onResume() {
        super.onResume()
        Handler().postDelayed({ model.requestUser() }, 1000)
    }

    override fun renderData(data: Boolean?) {
        data?.takeIf { it }?.let {
            startMainActivity()
        }
    }

    private fun startMainActivity() {
        MainActivity.start(this)
        finish()
    }
}