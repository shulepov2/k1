package com.geek.kotlin1

import android.app.Application
import com.geek.kotlin1.di.appModule
import com.geek.kotlin1.di.mainModule
import com.geek.kotlin1.di.noteModule
import com.geek.kotlin1.di.splashModule
import org.koin.android.ext.android.startKoin

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(appModule, splashModule, mainModule, noteModule))
    }
}